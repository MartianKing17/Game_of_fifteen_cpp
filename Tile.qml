import QtQuick 2.12
import QtQuick.Controls 2.12

Button {
    id: _tile
    property var number: ""
    property var color: "white"
    property alias border: _background.border
    text: number
    font.pixelSize: Math.min(width, height) / 2
    background: Rectangle {
        id: _background
        anchors.fill: _tile
        color: _tile.color
    }
}
