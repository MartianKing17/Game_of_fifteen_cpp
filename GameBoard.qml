﻿import QtQuick 2.0
import QtQuick.Window 2.14
import gameboard 1.0

GridView {
    id: _view
    interactive: false
    property bool isClickable: true
    signal gameOverWindow();

    model: GameBoardModel {
        id: boardModel
    }

    delegate: Tile {
        width: _view.cellWidth
        height: _view.cellHeight
        color: model.color
        border.color: "black"
        border.width: 1
        number: model.number
        visible: number !== ""
        onClicked: {
            if (_view.isClickable) {
                _view.model.move(index);
            }

            if (_view.model.checkingGameOver()) {
                gameOverWindow();
            }
        }
    }

    move: Transition {
        NumberAnimation { properties: "x,y"; duration: 400; easing.type: Easing.OutBounce }
    }
}


