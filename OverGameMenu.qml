import QtQuick 2.12
import QtQuick.Controls 2.12

Popup {
    id: _gameOverMenu
    readonly property real buttonHeight: height * 0.5 - _txt.height
    readonly property real buttonWidth: width * 0.35
    property var blurSource;
    signal restartClicked();
    closePolicy: Popup.NoAutoClose

    background: Rectangle {
        border.color: "black"
        border.width: 6
        radius: (width + height) / 64
        color: "grey"
        opacity: 0.95
    }

    contentItem: Item {
        Text {
            id: _txt
            anchors.centerIn: parent
            height: _gameOverMenu.height * 0.2
            text: "Would you like game again?"
            font.pixelSize: Math.min(parent.width, parent.height) / 20
        }

        GFButton {
            id: _restart
            text: "Restart"
            height: _gameOverMenu.buttonHeight
            width: _gameOverMenu.buttonWidth
            anchors.top: _txt.bottom
            anchors.left: parent.left
            anchors.leftMargin: _gameOverMenu.width * 0.1
            onClicked: {
                _gameOverMenu.close();
                _gameOverMenu.restartClicked();
            }
        }

        GFButton {
            text: "Exit"
            height: _gameOverMenu.buttonHeight
            width: _gameOverMenu.buttonWidth
            anchors.top: _txt.bottom
            anchors.left: _restart.right
            anchors.right: parent.right
            anchors.rightMargin: _gameOverMenu.width * 0.1
            anchors.leftMargin: _gameOverMenu.width * 0.1
            onClicked: { Qt.quit(); }
        }
    }
}

