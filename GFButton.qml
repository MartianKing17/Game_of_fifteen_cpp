import QtQuick 2.12
import QtQuick.Controls 2.12

Button {
    id: _button
    property bool isClickable: true
    hoverEnabled: true
    background: Rectangle {
        id: _background
        color: "#c7c7c7"
        anchors.fill: _button
        border.color: "black"
        border.width: 3
        radius: (_button.width + _button.height) / 64
    }

    onPressed: {
        if (isClickable) {
            _background.color = "#7f7f7f";
        }
    }
    onReleased: {
        if (isClickable) {
            _background.color = "#c7c7c7";
        }
    }
}
