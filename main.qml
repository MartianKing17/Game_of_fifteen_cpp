import QtQuick 2.10
import QtQuick.Window 2.10
import QtQuick.Controls 1.5
import gameboard 1.0

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Game of fifteen")
    property int numberOfCell: _view.model.getNumberOfCell()
    property int tileHeight: height / (Math.sqrt(numberOfCell) + 1)
    property int tileWidth: width / Math.sqrt(numberOfCell)

    OverGameMenu {
        id: _menu
        visible: false
        z: 1
        width: parent.width * 0.8
        height: parent.height * 0.8
        anchors.centerIn: parent
        blurSource: _view
        onRestartClicked: {
            _shuffle.clicked();
            _view.visible = true;
            _view.isClickable = true;
            _shuffle.visible = true;
        }
        onVisibleChanged: {
            if (visible) {
                _shuffle.isClickable = false;
                _view.isClickable = false;
            } else {
                _shuffle.isClickable = true;
                _view.isClickable = true;
            }
        }
    }

    GameBoard {
        id: _view
        width: parent.width
        height: tileHeight * Math.sqrt(numberOfCell)
        cellHeight: tileHeight
        cellWidth: tileWidth
        onGameOverWindow: {
            _menu.open();
        }
    }

    GFButton {
        id: _shuffle
        text: "Shuffle"
        width: parent.width
        height: parent.height - _view.height
        anchors.top: _view.bottom
        onClicked: {
            if (this.isClickable) {
                _view.model.shuffle();
            }
        }
    }
}
