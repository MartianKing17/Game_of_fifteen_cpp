#include "gameboard.h"
#include <iostream>
#include <random>
#include <cmath>
#include <algorithm>
#include <array>

GameBoard::GameBoard() : QAbstractItemModel()
{
    enterListOfElement();
}

QHash<int, QByteArray> GameBoard::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[color] = "color";
    roles[number] = "number";
    return roles;
}

QVariant GameBoard::data(const QModelIndex &index, int role = Qt::DisplayRole) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    int row = index.row();

    if (items.size() < row) {
        return QVariant();
    }

    switch (role) {
    case color:
           return QVariant(items.at(row).color);
    case number:
        return QVariant(items.at(row).number);
    default:
        break;
    }

    return QVariant();
}

QModelIndex GameBoard::parent(const QModelIndex &child) const
{
    return QModelIndex();
}

QModelIndex GameBoard::index(int row, int column, const QModelIndex &parent) const
{
    Element element;

    if (items.size() > row) {
        element = items.at(row);
    } else {
        element = {"", ""};
    }
    return createIndex(row, column, &element);
}

int GameBoard::columnCount(const QModelIndex &parent) const
{
    return parent.column();
}

int GameBoard::rowCount(const QModelIndex &parent = QModelIndex()) const
{
    return items.size();
}

int GameBoard::getNumberOfCell() const
{
    return len;
}

std::vector<int> GameBoard::randoming()
{
    int number;
    std::vector<int> arr;
    std::random_device rd;
    std::mt19937 generator(rd());
    std::uniform_int_distribution<> distribution(0, int(len - 1));

    while (arr.size() < len) {
        number = distribution(generator);

        if (std::find(arr.begin(), arr.end(), number) == arr.end()) {
            arr.push_back(number);
        }
    }

    return arr;
}

int GameBoard::summary(std::vector<int> arr)
{
    int sum = 0;
    int value = 0;
    auto iterOfEmptyCell = std::find(arr.begin(), arr.end(), 0);
    int indexOfEmptyCell = *iterOfEmptyCell + 1;
    arr.erase(iterOfEmptyCell);
    value = arr.at(0);
    sum += std::count_if(arr.begin() + 1, arr.end(), [&](const int i)
    {
        bool res = value < i;
        value = i;
        return res;
    });

    return sum + indexOfEmptyCell;
}

void GameBoard::enterListOfElement()
{
    std::size_t currectIndexOfColorArray = 0;
    const std::size_t lenOfColorArray = 3;
    QString color[lenOfColorArray] = {"#FF0000", "#00BFFF", "#9ACD32"};
    std::vector<int> arr;
    int sum = 1;

    while (sum % 2) {
        arr = randoming();
        sum = summary(arr);
    }

    if (items.size() > 0) {
        items.clear();
    }

    auto iter = std::find(arr.begin(), arr.end(), 0);
    emptyIndex = std::distance(arr.begin(), iter);

    Element element;
    beginInsertRows(QModelIndex(), items.size(), items.size());

    std::for_each(arr.begin(), arr.end(), [&](const int i)
    {
        if (i != 0) {
            currectIndexOfColorArray = currectIndexOfColorArray >= lenOfColorArray ? 0 : currectIndexOfColorArray;
            element = {color[currectIndexOfColorArray], QString().setNum(i)};
            ++currectIndexOfColorArray;
        } else {
            element = {"", ""};
        }

        items.push_back(element);
    });
    endInsertRows();
}

bool GameBoard::checkCorrectMovingIndex(std::size_t index1, std::size_t index2)
{
    return std::floor(index1 / std::sqrt(len)) == std::floor(index2 / std::sqrt(len));
}

void GameBoard::move(int index)
{
    int horizontal = index < emptyIndex ? 1 : 0;
    int vertical = index < emptyIndex ? 0 : 1;

    if (checkingPossibleTransition(emptyIndex, index))
    {
        if (std::abs(emptyIndex - index) == 1 && checkCorrectMovingIndex(emptyIndex, index))
        {
            beginMoveRows(QModelIndex(),index,index,QModelIndex(), emptyIndex + horizontal);
            endMoveRows();
            std::iter_swap(items.begin() + index, items.begin() + emptyIndex);
            emptyIndex = index;
        }
        else if (std::abs(emptyIndex - index) == std::sqrt(len))
        {
            beginMoveRows(QModelIndex(),index,index,QModelIndex(),emptyIndex);
            endMoveRows();
            beginMoveRows(QModelIndex(), emptyIndex + vertical, emptyIndex + vertical, QModelIndex(), index + vertical);
            endMoveRows();
            std::iter_swap(items.begin() + index, items.begin() + emptyIndex);
            emptyIndex = index;
        }
    }
}

bool GameBoard::checkingPossibleTransition(int firstIndex, int secondIndex)
{
    return (std::abs(firstIndex - secondIndex) == 1) || (std::abs(firstIndex - secondIndex) == std::sqrt(len));
}

bool GameBoard::checkingGameOver()
{
    std::vector<Element> correctVector = items;
    std::sort(correctVector.begin(), correctVector.end(), [](const Element first, const Element second)
    { return first.number.toUInt() < second.number.toUInt();});
    correctVector.push_back(correctVector.at(0));
    correctVector.erase(correctVector.begin());
    return std::equal(items.begin(), items.end(), correctVector.begin(), [](const Element first, const Element second)
    { return first.number.toUInt() == second.number.toUInt();});
}

void GameBoard::shuffle() {
    beginResetModel();
    enterListOfElement();
    endResetModel();
}

