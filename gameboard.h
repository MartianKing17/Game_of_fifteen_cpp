#ifndef GAMEBOARD_H
#define GAMEBOARD_H
#include <QAbstractItemModel>
#include <vector>
#include <QColor>

class GameBoard: public QAbstractItemModel {
    Q_OBJECT
public:
    GameBoard();
    Q_PROPERTY(const std::size_t len READ getNumberOfCell)
    Q_INVOKABLE void move(int index);
    Q_INVOKABLE void shuffle();
    Q_INVOKABLE bool checkingGameOver();
    bool checkCorrectMovingIndex(std::size_t index1, std::size_t index2);
    QHash<int, QByteArray> roleNames() const override;
    QVariant data(const QModelIndex &index, int role) const override;
    int rowCount(const QModelIndex &parent) const override;
    QModelIndex parent(const QModelIndex &child) const override;
    QModelIndex index(int row, int column, const QModelIndex &parent) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
public:
    enum GameBoardRoles {
        color = Qt::UserRole + 1,
        number
    };
private:
    // This function checking item[firtsIndex] above/under/left/right item[secondIndex]
    bool checkingPossibleTransition(int firstIndex, int secondIndex);
    void enterListOfElement();
    int summary(std::vector<int> arr);
    std::vector<int> randoming();
private:
    struct Element {
        QColor color;
        QString number;
    };
    int emptyIndex;
    std::vector<Element> items;
    const std::size_t len = 9;
public slots:
    int getNumberOfCell() const;
};

#endif // GAMEBOARD_H
